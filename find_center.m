function [label, center, bCon, sumD, D] = find_center(X)
    k = 1;
    while true
        [label, center, bCon, sumD, D] = litekmeans(X, k, 'Replicates', 10);

        aa = full(sum(center.*center,2));
        bb = full(sum(center.*center,2));
        ab = full(center*center');
        cD = bsxfun(@plus,aa,bb') - 2*ab;
        min_cd = min(min(cD(cD >= 0.1)));

        max_d = 0;
        for ii = 1:size(center, 1)
            d = D(label == ii, ii);
            max_d = max(max_d, max(d));
        end

        if k==1 || max_d < min_cd
            best_label = label;
            best_center = center;
            best_bCon = bCon;
            best_sumD = sumD;
            best_D = D;
            best_k = k;
        else
            break;
        end
        k = k + 1;
    end
    label = best_label;
    center = best_center;
    bCon = best_bCon;
    sumD = best_sumD;
    D = best_D;
    k = best_k;
end
