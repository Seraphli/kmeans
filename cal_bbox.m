function bbox = cal_bbox(pos, label, k)
    bbox = zeros(4, k);
    for ii = 1:k
        posx = pos(label == ii, 1);
        posy = pos(label == ii, 2);
        x = min(posx);
        y = min(posy);
        w = max(posx) - x;
        h = max(posy) - y;
        bbox(:, ii) = [x, y, w, h];
    end
end
