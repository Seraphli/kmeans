% center setting
center = [2:2:12;2:2:12]';
pos = zeros(length(center)*100, 2);
for ii = 1:size(center, 1)
    % random generate drift
    drift = rand(100,2);
    s_p = (ii - 1) * 100 + 1;
    e_p = s_p + 99;
    pos(s_p:e_p, :) = bsxfun(@plus, drift, center(ii,:));
end

cc=hsv(12);
figure(1);
hold on
for ii = 1:size(center, 1)
    s_p = (ii - 1) * 100 + 1;
    e_p = s_p + 99;
    scatter(pos(s_p:e_p,1), pos(s_p:e_p,2),50,cc(ii,:),'.');
end

% [label, center, bCon, sumD, D] = litekmeans(pos, 3, 'Replicates', 10);
% [label, center, bCon, sumD, D] = litekmeans(pos, 5);
[label, center, bCon, sumD, D] = find_center(pos);

scatter(center(:,1), center(:,2), 70);

bbox = cal_bbox(pos, label, size(center, 1));

for ii = 1:size(center, 1)
    rectangle('Position',bbox(:, ii));
end

hold off;
