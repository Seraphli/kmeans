function [ label , center , bCon , sumD , D ] = CFF_FastKmeans( X , K , varargin )
%LITEKMEANS K-means clustering, accelerated by matlab matrix operations.
%
%   label = CFF_FastKmeans(X, K) partitions the points in the N-by-P data matrix
%   X into K clusters.  This partition minimizes the sum, over all
%   clusters, of the within-cluster sums of point-to-cluster-centroid
%   distances.  Rows of X correspond to points, columns correspond to
%   variables.  KMEANS returns an N-by-1 vector label containing the
%   cluster indices of each point.
%
%   [label, center] = CFF_FastKmeans(X, K) returns the K cluster centroid
%   locations in the K-by-P matrix center.
%
%   [label, center, bCon] = CFF_FastKmeans(X, K) returns the bool value bCon to
%   indicate whether the iteration is converged.  
%
%   [label, center, bCon, SUMD] = CFF_FastKmeans(X, K) returns the
%   within-cluster sums of point-to-centroid distances in the 1-by-K vector
%   sumD.    
%
%   [label, center, bCon, SUMD, D] = CFF_FastKmeans(X, K) returns
%   distances from each point to every centroid in the N-by-K matrix D. 
%
%   [ ... ] = CFF_FastKmeans(..., 'PARAM1',val1, 'PARAM2',val2, ...) specifies
%   optional parameter name/value pairs to control the iterative algorithm
%   used by KMEANS.  Parameters are:
%
%   'Distance' - Distance measure, in P-dimensional space, that KMEANS
%      should minimize with respect to.  Choices are:
%            {'sqeuclidean'} - Squared Euclidean distance (the default)
%             'cosine'       - One minus the cosine of the included angle
%                              between points (treated as vectors). Each
%                              row of X SHOULD be normalized to unit. If
%                              the intial center matrix is provided, it
%                              SHOULD also be normalized.
%
%   'Start' - Method used to choose initial cluster centroid positions,
%      sometimes known as "seeds".  Choices are:
%         {'plusplus'} - select K centers based on kmeans++
%          'sample'  - Select K observations from X at random (the default)
%          'cluster' - Perform preliminary clustering phase on random 10%
%                      subsample of X.  This preliminary phase is itself
%                      initialized using 'sample'. An additional parameter
%                      clusterMaxIter can be used to control the maximum
%                      number of iterations in each preliminary clustering
%                      problem.
%           matrix   - A K-by-P matrix of starting locations; or a K-by-1
%                      indicate vector indicating which K points in X
%                      should be used as the initial center.  In this case,
%                      you can pass in [] for K, and KMEANS infers K from
%                      the first dimension of the matrix.
%
%   'MaxIter'    - Maximum number of iterations allowed.  Default is 100.
%
%   'Replicates' - Number of times to repeat the clustering, each with a
%                  new set of initial centroids. Default is 1. If the
%                  initial centroids are provided, the replicate will be
%                  automatically set to be 1.
%
% 'ClusterMaxIter' - Only useful when 'Start' is 'cluster'. Maximum number
%                    of iterations of the preliminary clustering phase.
%                    Default is 10.  
%
%
%    Examples:
%
%       fea = rand(500,10);
%       [label, center] = CFF_FastKmeans(fea, 5, 'MaxIter', 50);
%
%       fea = rand(500,10);
%       [label, center] = CFF_FastKmeans(fea, 5, 'MaxIter', 50, 'Replicates', 10);
%
%       fea = rand(500,10);
%       [label, center, bCon, sumD, D] = CFF_FastKmeans(fea, 5, 'MaxIter', 50);
%       TSD = sum(sumD);
%
%       fea = rand(500,10);
%       initcenter = rand(5,10);
%       [label, center] = CFF_FastKmeans(fea, 5, 'MaxIter', 50, 'Start', initcenter);
%
%       fea = rand(500,10);
%       idx=randperm(500);
%       [label, center] = CFF_FastKmeans(fea, 5, 'MaxIter', 50, 'Start', idx(1:5));

%% 检查输入参数的个数
if nargin < 2
    error('CFF_FastKmeans:TooFewInputs','At least two input arguments required.');
end

%% 
[n, p] = size(X);

%% get user specified parameters
% default parameters
opt.Distance = 'sqeuclidean' ;
opt.Start = 'plusplus' ;
opt.MaxIter = 100 ;
opt.Replicates = 1 ;
opt.ClusterMaxIter = 10 ;

[ error_id , error_msg , opt ] = ParseArgs( opt , varargin{:} ) ;
if ~isempty(error_id)
    error(sprintf('CFF_FastKmeans:%s',error_id),error_msg);
end

%% check parameters
% 'K'
if isempty( K ) 
    if ~isnumeric( opt.Start )
        error('CFF_FastKmeans:MissingK', 'You must specify the number of clusters, K.');
    end
else
    if ~(isscalar(K) && isnumeric(K) && isreal(K) && K > 0 && (round(K)==K))
        error('CFF_FastKmeans:InvalidK' , 'K must be a positive integer value.');
    elseif n < K
        error('CFF_FastKmeans:TooManyClusters', 'X must have more rows than the number of clusters.');
    end
end

% 'Distance'
if ischar( opt.Distance )
    distNames = { 'sqeuclidean' , 'cosine' };
    j = strcmpi( opt.Distance , distNames );
    j = find(j);
    if length(j) > 1
        error('CFF_FastKmeans:AmbiguousDistance', 'Ambiguous ''Distance'' parameter value:  %s.', opt.Distance );
    elseif isempty(j)
        error('CFF_FastKmeans:UnknownDistance', 'Unknown ''Distance'' parameter value:  %s.', opt.Distance );
    end
else
    error('CFF_FastKmeans:InvalidDistance', 'The ''Distance'' parameter value must be a string.');
end

% 'Start'
center = [];
if ischar( opt.Start )
    startNames = { 'plusplus' , 'sample' , 'cluster' };
    j = find( strcmpi( opt.Start , startNames ) ) ;
    if length(j) > 1
        error(message('CFF_FastKmeans:AmbiguousStart', opt.Start));
    elseif isempty(j)
        error(message('CFF_FastKmeans:UnknownStart', opt.Start));
    end
elseif isnumeric( opt.Start )
    if size( opt.Start , 2 ) == p
        center = opt.Start;
    elseif ( size( opt.Start , 2 ) == 1 || size( opt.Start , 1 ) == 1)
        center = X(opt.Start,:);
    else
        error('CFF_FastKmeans:MisshapedStart', 'The ''Start'' matrix must have the same number of columns as X.');
    end
    
    if ~isempty( center )
        if isempty(K)
            K = size(center,1);
        elseif (K ~= size(center,1))
            error('CFF_FastKmeans:MisshopedStart', 'The ''Start'' matrix must have K rows.');
        end
    end
    
    opt.Start = 'numeric';
else
    error('CFF_FastKmeans:InvalidStart', 'The ''Start'' parameter value must be a string or a numeric matrix or array.');
end
        
% 'MaxIter'
if ~( isnumeric( opt.MaxIter ) && isscalar( opt.MaxIter ) && ( opt.MaxIter > 0 ) && (round(opt.MaxIter)==opt.MaxIter) )
    error('CFF_FastKmeans:InvalidMaxIter' , 'MaxIter must be a positive integer value.');
end

% 'clusterMaxIter', The maximum iteration number for preliminary clustering phase on random
if ~( isnumeric( opt.ClusterMaxIter ) && isscalar( opt.ClusterMaxIter ) && ( opt.ClusterMaxIter > 0 ) && (round(opt.ClusterMaxIter)==opt.ClusterMaxIter) )
    error('CFF_FastKmeans:InvalidClusterMaxIter' , 'ClusterMaxIter must be a positive integer value.');
end

% Replicates
if ~( isnumeric( opt.Replicates ) && isscalar( opt.Replicates ) && ( opt.Replicates > 0 ) && (round(opt.Replicates)==opt.Replicates) )
    error('CFF_FastKmeans:InvalidReplicates' , 'Replicates must be a positive integer value.');
end

%% process
data_type = class(X) ;
bestlabel = [];
bCon = false;

for t=1:opt.Replicates
    %% initial centers
    switch opt.Start
        case 'sample'
            center = X( randsample(n,K) , : );
        case 'cluster'
            num_subsample = round( min( n , max( .1*n , 2*K ) ) ) ; % modified by cff
            Xsubset = X(randsample(n,num_subsample),:); % modified by cff
            [ ~ , center ] = CFF_FastKmeans( Xsubset , K , varargin{:} , 'Start' , 'sample' , 'Replicates' , 1  , 'MaxIter' , opt.ClusterMaxIter ) ;
            clear Xsubset ;
        case 'plusplus'
            % The k-means++ initialization.
            center = X( 1+round(rand*(n-1)) , : ) ;
            switch opt.Distance
                    case 'sqeuclidean'
                        D = bsxfun( @minus , X , center ) ;
                        D = sqrt( sum( D .* D , 2 ) ) ;
                    case 'cosine'
                        D = 1- X * center' ;
            end   
                
            for i = 2:K
                pD = min( D , [] , 2 ) ;
                pD = cumsum( pD );
                if pD(end) == 0, 
                    center(i:k,:) = X( ones(1,k-i+1) , : ); 
                    break ; 
                end
                pD = pD/pD(end) ;
                center(i,:) = X( find(rand<pD,1) , : ) ;
                switch opt.Distance
                    case 'sqeuclidean'
                        tmp = bsxfun( @minus , X , center(i,:) ) ;
                        D( : , i ) = sqrt( sum( tmp .* tmp , 2 ) ) ;
                    case 'cosine'
                        D( : , i ) = 1- X * center(i,:)' ;
                end                
            end
        case 'numeric'
    end
    
    %% 

    switch opt.Distance
        case 'sqeuclidean'
            last = 0;
            it=0;
            label = ones(n,1) ;
            D = ones( n , K , data_type ) ;
            old_center = zeros( K , p , data_type ) ;
            while any(label ~= last) && it<opt.MaxIter
                % get the changed centers
                changed_center_label = find( sum( abs(old_center-center)>eps , 2 ) > 0 ) ;
%                 fprintf( 'num_changed: %d\n' , length(changed_center_label) ) ;
                if isempty(changed_center_label)
                    break ;
                end
                
                % keep the latest status
                last = label;
                old_center = center ;
                
                % updata distance matrix
                D(:,changed_center_label) = bsxfun( @minus , sum(center(changed_center_label,:).*center(changed_center_label,:),2)' , 2*(X*center(changed_center_label,:)' ) ) ;
                
                % get new labels of points
                [min_D,label] = min(D,[],2); % assign samples to the nearest centers
                
                % handle the missing center
                ll = unique(label);
                if length(ll) < K
                    missCluster = 1:K;
                    missCluster(ll) = [];
                    missNum = length(missCluster);
                    [~,idx] = sort(min_D,1,'descend');
                    label(idx(1:missNum)) = missCluster;
                end
                
                % updata the centers
                E = sparse(1:n,label,1,n,K,n);  % transform label into indicator matrix
                center = full((E*spdiags(1./sum(E,1)',0,K,K))'*double(X) );    % compute center of each cluster   % modified by cff
                it=it+1;
            end
            
            % check whether kmeans converges
            if it<opt.MaxIter
                bCon = true;
            end
            
            % re-compute the distance
            if it<opt.MaxIter
                D = bsxfun( @plus , sum(X.*X,2) , D ) ;
            else
                D = bsxfun( @plus , sum(X.*X,2) , sum(center.*center,2)' ) - 2*(X*center') ;
            end
            D(D<0) = 0 ;
            D = sqrt(D) ;
            E = sparse(1:n,label,1,n,K,n);  % transform label into indicator matrix
            sumD = full( sum( E .* double(D) , 1 ) ) ;
                   
            %
            if isempty(bestlabel)
                bestlabel = label;
                bestcenter = center;
                bestsumD = sumD;
                bestD = D;
            else
                if sum(sumD) < sum(bestsumD)
                    bestlabel = label;
                    bestcenter = center;
                    bestsumD = sumD;
                    bestD = D;
                end
            end
        case 'cosine'
            last = 0;
            it=0;
            label = ones(n,1) ;
            D = ones( n , K , data_type ) ;
            old_center = zeros( K , p , data_type ) ;
            while any(label ~= last) && it<opt.MaxIter
                % get the changed centers
                changed_center_label = find( sum( abs(old_center-center)>eps , 2 ) > 0 ) ;
%                 fprintf( 'num_changed: %d\n' , length(changed_center_label) ) ;
                if isempty(changed_center_label)
                    break ;
                end
                
                % keep the latest status
                last = label;
                old_center = center ;
                
                % updata distance matrix
                D(:,changed_center_label) = 1 - X*center(changed_center_label,:)' ;
                
                % get new labels of points
                [min_D,label] = min(D,[],2); % assign samples to the nearest centers
                
                % handle the missing center
                ll = unique(label);
                if length(ll) < K
                    missCluster = 1:K;
                    missCluster(ll) = [];
                    missNum = length(missCluster);
                    [~,idx] = sort(min_D,1,'descend');
                    label(idx(1:missNum)) = missCluster;
                end
                
                % updata the centers                
                E = sparse(1:n,label,1,n,K,n);  % transform label into indicator matrix
                center = full((E*spdiags(1./sum(E,1)',0,K,K))'*double(X));    % compute center of each cluster
                centernorm = sqrt(sum(center.^2, 2));
                center = center ./ centernorm(:,ones(1,p));
                it=it+1;
            end
            % check whether kmeans converges
            if it<opt.MaxIter
                bCon = true;
            end
            
            % re-compute the distance
            if it>=opt.MaxIter
                D = 1 - X*center' ;
            end
            E = sparse(1:n,label,1,n,K,n);  % transform label into indicator matrix
            sumD = full( sum( E .* double(D) , 1 ) ) ;
            
            %
            if isempty(bestlabel)
                bestlabel = label;
                bestcenter = center;
                bestsumD = sumD;
                bestD = D;
            else
                if sum(sumD) < sum(bestsumD)
                    bestlabel = label;
                    bestcenter = center;
                    bestsumD = sumD;
                    bestD = D;
                end
            end
    end
end

label = bestlabel;
center = bestcenter;
sumD = bestsumD;
D = bestD;


end

function [ error_id , error_msg , opt ] = ParseArgs( dflt_opt , varargin )
%% optional parameters
% opt.Distance
% opt.Start
% opt.MaxIter
% opt.Replicates
% opt.clusterMaxIter

error_msg = '';
error_id = '';
opt = dflt_opt ;
nargs = length(varargin);

if mod(nargs,2)~=0
    error_id = 'WrongNumberArgs';
    error_msg = 'Wrong number of arguments.';
else
    for j=1:2:nargs
        arg_name = varargin{j} ;
        
        if ~ischar(arg_name)
            error_id = 'BadParamName';
            error_msg = 'Parameter name must be text.';
            break;
        end
        
        is_param_valid = false ;
        if strcmpi( arg_name , 'Distance' )
            opt.Distance = varargin{j+1} ;
            is_param_valid = true ;
        end
        if strcmpi( arg_name , 'Start' )
            opt.Start = varargin{j+1} ;
            is_param_valid = true ;
        end
        if strcmpi( arg_name , 'MaxIter' )
            opt.MaxIter = varargin{j+1} ;
            is_param_valid = true ;
        end
        if strcmpi( arg_name , 'Replicates' )
            opt.Replicates = varargin{j+1} ;
            is_param_valid = true ;
        end
        if strcmpi( arg_name , 'ClusterMaxIter' )
            opt.ClusterMaxIter = varargin{j+1} ;
            is_param_valid = true ;
        end
           
        if ~is_param_valid
            error_id = 'BadParamName';
            error_msg = sprintf('Invalid parameter name:  %s.',arg_name);
            break;
        end
%         if ~isfield( opt , arg_name )
%             error_id = 'BadParamName';
%             error_msg = sprintf('Invalid parameter name:  %s.',arg_name);
%             break;
%         else
%             opt.(arg_name) = varargin{j+1} ;
%         end      
    end
end
    
end